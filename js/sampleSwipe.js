require([
  "esri/Map",
  "esri/layers/FeatureLayer",
  "esri/widgets/Editor",
  "esri/views/MapView",
  "esri/popup/content/AttachmentsContent",
  "esri/popup/content/TextContent"
], function(Map, FeatureLayer, Editor, MapView, AttachmentsContent,TextContent) {

  var map = new Map({
    basemap: "gray"
  });

  var featureLayer = new FeatureLayer({
    url:
    "https://services.arcgis.com/HRPe58bUyBqyyiCt/arcgis/rest/services/Fairfax_Buildings_Gov/FeatureServer",
    outFields: ["*"],
  });
  map.add(featureLayer);

  var view = new MapView({
    container: "mapViewDiv",
    map: map,
    center: [-77.311920,38.847817],
    zoom: 11
  });

  view.when(function(){
    view.popup.autoOpenEnabled = false;// no popups
  });

  let editor = new Editor({
    view: view
  });
  view.ui.add(editor, "top-right");






});
