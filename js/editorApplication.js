require([
    // ArcGIS
    "esri/Map",
    "esri/layers/FeatureLayer",
    "esri/layers/GraphicsLayer",
    "esri/views/MapView",
    "esri/PopupTemplate",

    // Widgets
    "esri/widgets/Home",
    "esri/widgets/Zoom",
    "esri/widgets/Compass",
    "esri/widgets/Search",
    "esri/widgets/Legend",
    "esri/Basemap",
    "esri/widgets/BasemapToggle",
    "esri/layers/VectorTileLayer",
    "esri/widgets/ScaleBar",
    "esri/widgets/Attribution",
    "esri/widgets/Swipe",
    "esri/widgets/Editor",
    "esri/widgets/Popup",
    "esri/widgets/TimeSlider",
    "esri/widgets/LayerList",
    "esri/tasks/support/Query",
    "esri/geometry/geometryEngine",
    "esri/geometry/geometryEngineAsync",
    "esri/Graphic",
    "esri/Color",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/renderers/SimpleRenderer",
    "bootstrap/Collapse",
    "bootstrap/Dropdown",

    // Calcite Maps
    "calcite-maps/calcitemaps-v0.10",

    // Calcite Maps ArcGIS Support
    "calcite-maps/calcitemaps-arcgis-support-v0.10",
    "dojo/on",
    "dojo/promise/all",
    "dojo/query",
    "dojo/dom",
    "dojo/domReady!"
  ], function(Map, FeatureLayer,GraphicsLayer,MapView,PopupTemplate, Home, Zoom, Compass, Search, Legend,Basemap, BasemapToggle, VectorTileLayer, ScaleBar, Attribution, Swipe,Editor,Popup,TimeSlider,LayerList,Query,geometryEngine,geometryEngineAsync,Graphic,Color,SimpleFillSymbol, SimpleLineSymbol, SimpleRenderer,Collapse, Dropdown, CalciteMaps, CalciteMapArcGISSupport,on,all,query,dom) {

    /******************************************************************
     *
     * Create the map, view and widgets
     *
     ******************************************************************/
     var closeSwipe = dom.byId("swipeOff");
     var editTab = dom.byId("editNav");
     var swipeTab = dom.byId("swipeNav");
     var analysisTab = dom.byId("analysisNav");
     var featureTab = dom.byId("featHunterNav");

     // Workaround since the tabs/list items cant share the same ID
     var editTabList = dom.byId("editNavList");
     var swipeTabList = dom.byId("swipeNavList");
     var analysisTabList = dom.byId("analysisNavList");
     var featureTabList = dom.byId("featHunterNavList");

     var panelChange = query(".panel-body")[0];

     var editorInfo = "<p>The editor tab is used to make changes to government data. " +
     " to edit, add, or delete a feature, use the widget located in the bottom right of the application</p>";

     var swipeInfo = "<p>Swipe is used to compare and contrast OSM and Government data in a single map frame." +
     " Use the slider to interact with the map, and click on features to see their attribution.<br> NOTE: " +
     "Government data is on the left, and OpenStreetMap is on the right. </p>";

     var analysisInfo = "<p>Time Slider uses a time slider widget to view specifically the OSM data." +
     " Use the slider in the bottom left to animate the map and discover when features were added.<br> NOTE: " +
     "Looking at data from 2018-2019 may show new additions within OSM that are missing from government data. </p>";


     var featureCompInfo = "<p>Feature comparison uses a geometry query on a two mile buffer to analyze features" +
     " on a local scale. The returned results will show how many features are in one dataset versus another.<br> NOTE: " +
     "Upon double-clicking, a doughnut chart will be generated with graphic symbology reflecting what is seen on the map. </p>";

     let featureCompareEvent; // Click event for feat comparison

     isOsm = true;

     var myDoughnutChart;

     var resultsLayer = new GraphicsLayer();
     
     // Counters for the feature comparison mode.
     let govGraphicCount = 0;
     let osmGraphicCount = 0;
     let osmCounter = 0;
     let govCounter = 0;
     let overlapCounter = 0;

     // Buffer color
     var buffSym = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 255, 255, 1]), 3), null);
     var buffSymFade = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 255, 255, 0.4]), 10), null);
     var geoDiffSym = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 255, 0, 0.5]), 2), null);
     var geoDiffSymGov = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0, 0.5]), 2), null);
     var osmLineSym = {type:"simple-line",color:[216,179,101],width:"3px",style:"dash"};
     var govLineSym = {type:"simple-line",color:[90,180,172],width:"3px",style:"dash"};
     var osmPolySym = {type:"simple-fill",color:[216,179,101],style:"solid",outline:{color:"black",width:1}};
     var govPolySym = {type:"simple-fill",color:[90,180,172],style:"solid",outline:{color:"black",width:1}};


     let osmParkGraphics;
     let govParkGraphics;
     let geoBuffer;

     var govFootprintTemplate = {
      title:"Government Building",
      content:[
        {type:"fields",
        fieldInfos:[
          {
            fieldName: "NAME",
            label: "Name"
          },
          {
           fieldName: "EFFECTIVE_DATE",
           label: "Created"
         },
         {
           fieldName: "TYPE",
           label: "Type"
         },
         {
           fieldName: "SOURCE",
           label: "Source"
         },

        ]
       }
      ]
  }

     var govParkTemplate = {
       title:"Government Park",
       content:[
         {type:"fields",
         fieldInfos:[
           {
             fieldName: "NAME",
             label: "Name"
           },
           {
            fieldName: "CreationDa",
            label: "Created"
          },
          {
            fieldName: "Editor",
            label: "Edited By"
          },
          {
            fieldName: "PARK_NAME",
            label: "Alt Name"
          },

         ]
        }
       ]
   }

   var govTrailsTemplate = {
    title:"Government Trail",
    content:[
      {type:"fields",
      fieldInfos:[
        {
          fieldName: "TRAIL_NAME",
          label: "Name"
        },
        {
         fieldName: "CreationDa",
         label: "Created"
       },
       {
         fieldName: "MAINTENANC",
         label: "Maintained By"
       },
       {
         fieldName: "SURFACE_MA",
         label: "Surface"
       },
       {
        fieldName: "WIDTH",
        label: "Trail Width"
      },
      {
        fieldName: "COMMENTS",
        label: "Notes"
      },

      ]
     }
    ]
}

var osmTrailsTemplate = {
  title:"OSM Trail",
  content:[
    {type:"fields",
    fieldInfos:[
      {
        fieldName: "name",
        label: "Name"
      },
      {
       fieldName: "osm_timest",
       label: "Created"
     },
     {
       fieldName: "operator",
       label: "Maintained By"
     },
     {
       fieldName: "surface",
       label: "Surface"
     },
     {
      fieldName: "bicycle",
      label: "Biking Allowed"
    },
    {
      fieldName: "bridge",
      label: "Bridge"
    },
    {
      fieldName: "trail_visi",
      label: "Visibility"
    },
    {
      fieldName: "width",
      label: "Width"
    },
    {
      fieldName: "access",
      label: "Access Limits"
    },

    ]
   }
  ]
}

var osmParkTemplate = {
  title:"OSM Park",
  content:[
    {type:"fields",
    fieldInfos:[
      {
        fieldName: "name",
        label: "Name"
      },
      {
       fieldName: "osm_timest",
       label: "Created"
     },
     {
       fieldName: "operator",
       label: "Maintained By"
     },
     {
       fieldName: "landuse",
       label: "Purpose"
     },
     {
      fieldName: "other_tags",
      label: "Misc Info"
    },
    {
      fieldName: "COMMENTS",
      label: "Notes"
    },

    ]
   }
  ]
}

var osmFootprintTemplate = {
  title:"OSM Building",
  content:[
    {type:"fields",
    fieldInfos:[
      {
        fieldName: "name",
        label: "Name"
      },
      {
       fieldName: "osm_timest",
       label: "Created"
     },
     {
       fieldName: "building",
       label: "Type"
     },
     {
       fieldName: "tourism",
       label: "Purpose"
     },
     {
      fieldName: "other_tags",
      label: "Misc Info"
    },

    ]
   }
  ]
}
     // Style for light gray vector basemap (removed building footprints and park parcels)
     var basemapLayer = new VectorTileLayer({
       url: "https://uw-mad.maps.arcgis.com/sharing/rest/content/items/14d0b230306d4069abc932fe71051e84/resources/styles/root.json?f=pjson"
     })

     // Creating a new basemap object with input layers lets me add this to keep the toggle working
     var vtBasemap = new Basemap({baseLayers:[basemapLayer]});

     var map = new Map({
       basemap:vtBasemap
     });

     //map.add(basemapLayer);

     var parksRenderer = {type:"simple",symbol:{type:"simple-fill",color:[0,100,25,.70],style:"solid"}};
     var trailsRenderer = {type:"simple",symbol:{type:"simple-line",color:[168,56,0,.85],width:0.4,style:"long-dash"}};
     var footprintRenderer = {type:"simple",symbol:{type:"simple-fill",color:[0,80,80,.7],/*color:[0,165,165,.70]*/style:"solid"}};

     var govFootprints = new FeatureLayer({
      url:"https://services1.arcgis.com/ioennV6PpG5Xodq0/arcgis/rest/services/OpenData_S10/FeatureServer/0/query?outFields=*&where=1%3D1",
      outFields: ["*"],
      popupTemplate: govFootprintTemplate,
      renderer: footprintRenderer,
      title: "Gov_Footprints"
    });

    var osmFootprints = new FeatureLayer({
      url:"https://services.arcgis.com/HRPe58bUyBqyyiCt/arcgis/rest/services/OSM_Footprints/FeatureServer",
      outFields: ["*"],
      popupTemplate: osmFootprintTemplate,
      renderer: footprintRenderer,
      title: "OSM_Footprints"
    });


     var osmParks = new FeatureLayer({
       url:"https://services.arcgis.com/HRPe58bUyBqyyiCt/arcgis/rest/services/Fairfax_OSM_Parks/FeatureServer",
       outFields: ["*"],
       popupTemplate: osmParkTemplate,
       renderer: parksRenderer,
       title: "OSM_Parks"
     });

     var govParks = new FeatureLayer({
       url:"https://services.arcgis.com/HRPe58bUyBqyyiCt/arcgis/rest/services/Fairfax_County_Gov_Parks/FeatureServer",
       outFields:["*"],
       popupTemplate: govParkTemplate,
       renderer: parksRenderer,
       title: "Gov_Parks"
     });

     var osmTrails = new FeatureLayer({
       url:"https://services.arcgis.com/HRPe58bUyBqyyiCt/arcgis/rest/services/Fairfax_County_OSM_Trails/FeatureServer",
       outFields: ["*"],
       popupTemplate: osmTrailsTemplate,
       renderer: trailsRenderer,
       title: "OSM_Trails",
       timeInfo:{
         startField:"osm_timest",//date field
         interval: {
           unit:"months",
           value:1
         }
       }
     });

     var govTrails = new FeatureLayer({
       url:"https://services.arcgis.com/HRPe58bUyBqyyiCt/arcgis/rest/services/Fairfax_County_Gov_Trails/FeatureServer",
       outFields: ["*"],
       popupTemplate: govTrailsTemplate,
       renderer: trailsRenderer,
       title: "Gov_Trails"
     });

     map.add(osmParks);
     map.add(osmTrails);
     map.add(osmFootprints);
     map.add(govParks);
     map.add(govTrails);
     map.add(govFootprints);
     console.log(govTrails.title);
     //map.add(resultsLayer);

     let allSourceLayers = [osmParks,osmTrails,govParks,govTrails];

     // layer views (book-keeping for all graphics)
     let osmParksLayerView;
     let govParksLayerView;
     let osmTrailsLayerView;
     let govTrailsLayerView;
     let osmFootprintsLayerView;
     let govFootprintsLayerView;
    

    // View
    var mapView = new MapView({
      container: "mapViewDiv",
      map: map,
      center: [-77.300227,38.843320],
      zoom: 12
      //padding: {
        //top: 50,
        //bottom: 0
      //},
      //ui: {components: []}
    });

    //mapView.defaultPopupTemplateEnabled = true;
    mapView.popup.defaultPopupTemplateEnabled = true;
    mapView.popup.autoOpenEnabled = true; // set to false in edit mode
    // dock the popup along the bottom to stay out of the way
    //mapView.popup.alignment = "bottom-center";
    mapView.popup.dockEnabled = true;
    mapView.popup.dockOptions.position = "bottom-center";
    mapView.popup.dockOptions.breakpoint = false;
    //mapView.popup.currentDockPosition = "bottom-center";
    //mapView.popup.dockOptions.buttonEnabled = false;
    //mapView.popup.dockOptions.position = "bottom-center";
    // disable double-click to zoom (used for feature compare tool)
    mapView.on("double-click", function(evt){
      evt.stopPropagation();
    });

    // access graphics for the feature layers
    mapView.whenLayerView(osmParks).then(lv => osmParksLayerView = lv);
    mapView.whenLayerView(govParks).then(lv => govParksLayerView = lv);
    mapView.whenLayerView(osmTrails).then(lv => osmTrailsLayerView = lv);
    mapView.whenLayerView(govTrails).then(lv => govTrailsLayerView = lv);
    mapView.whenLayerView(osmFootprints).then(lv => osmFootprintsLayerView = lv);
    mapView.whenLayerView(govFootprints).then(lv => govFootprintsLayerView = lv);



    var swipe = initSwipe();

    let layerList = new LayerList({
      view:mapView
    });

    // Configure what layers get shown
    /*let layerListFeatComp = new LayerList({
      view:mapView,
      container: document.createElement("div"),
    });*/

    mapView.ui.add(swipe); // swipe is added by default
    mapView.ui.add(layerList,{position:"bottom-left"});
    let statusTab = "swipeTab";

    // Editor to interface with County data
    let editor = new Editor({
      view: mapView
    });

    console.log(map.layers);

    // initialize timeslider, but don't add
    let timeSlider;

    // Remove swipe, add the editor widget
    on(editTab, "click", function(evt){
      panelChange.innerHTML = editorInfo;
      if (statusTab === "editTab" || statusTab === "editTabList"){
        return;
      }
      clearMap("editTab");
      // does not working coming from "findFeatures" tab
      console.log("add editor");
      mapView.popup.autoOpenEnabled = false;
      mapView.ui.add(editor,"bottom-right");
    });

    on(editTabList, "click", function(evt){
      panelChange.innerHTML = editorInfo;
      if (statusTab === "editTab" || statusTab === "editTabList"){
        return;
      }
      clearMap("editTab");
      // does not working coming from "findFeatures" tab
      console.log("add editor");
      mapView.popup.autoOpenEnabled = false;
      mapView.ui.add(editor,"bottom-right");
    });

    on(swipeTab,"click",function(evt){
      panelChange.innerHTML = swipeInfo;
      if (statusTab === "swipeTab" || statusTab === "swipeTabList"){
        return;
      }
      clearMap("swipeTab");
      mapView.popup.autoOpenEnabled = true;
      // re-create the swipe in case feat compare messed up the view?
      /*swipe = new Swipe({
        leadingLayers: [govParks,govTrails,govFootprints],
        trailingLayers: [osmParks, osmTrails,osmFootprints],
        position: 50,
        view: mapView
      });*/
      mapView.ui.add(swipe);
    });

    on(swipeTabList,"click",function(evt){
      panelChange.innerHTML = swipeInfo;
      if (statusTab === "swipeTab" || statusTab === "swipeTabList"){
        return;
      }
      clearMap("swipeTab");
      mapView.popup.autoOpenEnabled = true;
      // re-create the swipe in case feat compare messed up the view?
      /*swipe = new Swipe({
        leadingLayers: [govParks,govTrails,govFootprints],
        trailingLayers: [osmParks, osmTrails,osmFootprints],
        position: 50,
        view: mapView
      });*/
      mapView.ui.add(swipe);
    });

    on(analysisTab,"click",function(evt){
      panelChange.innerHTML = analysisInfo;
      if (statusTab === "analysisTab" || statusTab === "analysisTabList"){
        return;
      }
      // Remove swipe
      // Remove editor widget
      // Add OSM layer, and filtering
      clearMap("analysisTab");
      mapView.popup.autoOpenEnabled = true;
      initTimeSlider();
      mapView.ui.add(timeSlider,"bottom-left");

    });

    on(analysisTabList,"click",function(evt){
      panelChange.innerHTML = analysisInfo;
      if (statusTab === "analysisTab" || statusTab === "analysisTabList"){
        return;
      }
      // Remove swipe
      // Remove editor widget
      // Add OSM layer, and filtering
      clearMap("analysisTab");
      mapView.popup.autoOpenEnabled = true;
      initTimeSlider();
      mapView.ui.add(timeSlider,"bottom-left");

    });

    on(featureTab,"click",function(evt){
      panelChange.innerHTML = featureCompInfo;
      if (statusTab === "featureTab" || status === "featureTabList"){
        return;
      }
      clearMap("featureTab");
      mapView.popup.autoOpenEnabled = true;
      featureCompareEvent = on.pausable(mapView,"double-click",function(evt){
      executeFeatureCompare(evt);

      });

    });


    on(featureTabList,"click",function(evt){
      panelChange.innerHTML = featureCompInfo;
      if (statusTab === "featureTab" || statusTab === "featureTabList"){
        return;
      }
      clearMap("featureTab");
      mapView.popup.autoOpenEnabled = true;
      featureCompareEvent = on.pausable(mapView,"double-click",function(evt){
        executeFeatureCompare(evt);

      });

    });

    function executeFeatureCompare(evt){
       //clear previous graphics
       mapView.graphics.removeAll();
        
       // if canvas element isn't created, create
       if(!query("#chartjs")[0]){

       var canvasElement = document.createElement("canvas");
       canvasElement.id = "chartjs";
       panelChange.appendChild(canvasElement);

       }

       // buffer calc
       // query feats within buffer bounds (osm and gov)
       // using resulting graphics, find overlapping feats, then a difference calc
       resultsLayer.removeAll();
       clearCounts();
       geometryEngineAsync.geodesicBuffer(evt.mapPoint,0.5,"miles")
       .then(addBufferGraphic)
       //.then(findOSMGraphics)
       //.then(findGovGraphics)
       .then(findGovLVGraphics)
       .then(findMatchingFeats)
       .then(initFeatCompChart)
       //.then(performHitTest)
       //.then(resolveHit)
       //.then(findGeoDifferenceOSM)
       //.then(renderDiffOSM)
       //.then(findGeoDifferenceGov)
       //.then(renderDiffGov)
       //.then(initFeatCompChart) // render pie chart
       .catch(function(err){console.error(err)});

       

    }
    // Query all features based on layer views within the buffer
    // use these results to perform the intersection/overlap logic
    function findGovLVGraphics(){
      // OSM graphics within buffer
      var queryList = [];

      // your list of queries is dependent on what layers are turned on in layerlist
      console.log(govFootprintsLayerView);
      console.log(govParksLayerView);

      // Only query the layers that are set to be "true"

      var query = new Query();
      query.geometry = geoBuffer;
      query.spatialRelationship = "intersects";
      query.returnGeometry = true;


      // only include visible pairs for the geometry queries
      if(govParksLayerView.visible){
        queryList.push(govParksLayerView.queryFeatures(query));
        queryList.push(osmParksLayerView.queryFeatures(query));
      }

      if (govTrailsLayerView.visible){
        queryList.push(govTrailsLayerView.queryFeatures(query));
        queryList.push(osmTrailsLayerView.queryFeatures(query));

      }

      if (govFootprintsLayerView.visible){
        queryList.push(govFootprintsLayerView.queryFeatures(query));
        queryList.push(osmFootprintsLayerView.queryFeatures(query));
      }

      return queryList;

    }

    function findMatchingFeats(results){
      // You have 4 arrays of features
      // parks -> 0 = gov, 1 = osm
      // trails -> 2 = gov, 3 = osm 
      // buildings -> 4 = gov, 5 = osm

      all(results).then(function(ab){
        console.log(ab);
        // 0 - 3 options for graphics
        switch(ab.length){
          case 0:
            break;
          case 2:
            mapView.graphics.addMany(findMatchingGeo(ab[0],ab[1]));
            break;
          case 4:
            mapView.graphics.addMany(findMatchingGeo(ab[0],ab[1]));
            mapView.graphics.addMany(findMatchingGeo(ab[2],ab[3]));
            break;
          case 6:
            mapView.graphics.addMany(findMatchingGeo(ab[0],ab[1]));
            mapView.graphics.addMany(findMatchingGeo(ab[2],ab[3]));
            mapView.graphics.addMany(findMatchingGeo(ab[4],ab[5]));
            break;
          default:
            console.log("no match");
        }
        initFeatCompChart();
      });
            
        
      }


    function findMatchingGeo(gov,osm){

      symbologyGraphics = []

      for (x = 0; x < gov.features.length; x++){
        hasOverlap = false;
        for (y = 0; y < osm.features.length; y++){
          if (geometryEngine.intersects(gov.features[x].geometry, osm.features[y].geometry)){
            //console.log("intersecting feature");
            hasOverlap = true;
            overlapCounter++;
            osm.features[y].attributes["matches"] = true;
            break;
          }
        }
        if(!hasOverlap){
          console.log("unique gov feature");
          govCounter++;
          // type
          if (gov.geometryType === "polygon"){
            symbologyGraphics.push(new Graphic(gov.features[x].geometry, govPolySym));
          }
          else{
            symbologyGraphics.push(new Graphic(gov.features[x].geometry, govLineSym));
          }
        }
      }

      // count the number of unique OSM features
      for (z = 0; z < osm.features.length; z++){
        if (osm.features[z].attributes.matches != true){
          osmCounter++;
          if (osm.geometryType === "polygon"){
            //console.log("polygon graphic");
            symbologyGraphics.push(new Graphic(osm.features[z].geometry, osmPolySym));
          }
          else{
            symbologyGraphics.push(new Graphic(osm.features[z].geometry, osmLineSym));
          }
        }
      }

      return symbologyGraphics;
    }
    

    function storeResults(results){

      govParkGraphics = [];

      // store the graphics in a list
      results.features.forEach(function(inputGraphic){
        govParkGraphics.push(inputGraphic);
      });

      return govParkGraphics;

    }

    function performHitTest(results){

      // for each graphic, perform a hit test using its centroid 
      // when a match is found, ensure it's part of opposite dataset and perform
      // an intersect
      console.log(results);
      var hitTestQueries = [];

      var queries = results.map(function(inputGeom){
        centroid = inputGeom.geometry.centroid;
        console.log(centroid);
        console.log(mapView);
        return mapView.hitTest(centroid);
      });

      all(queries).then(function(results){
        console.log(results);
        return results;

      });

    }



    function resolveHit(results){

      console.log(results);


      
    }

    // Add the buffered geometry as a graphic to the map
    function addBufferGraphic(bufferedGeo){
      geoBuffer = bufferedGeo;
      return mapView.graphics.add(new Graphic(bufferedGeo, buffSymFade));
    }
    // Execute a query to find OSM graphics within the buffer
    function findOSMGraphics(inputLayer){
      console.log("query start");
      var query = osmParks.createQuery();
      //console.log(geoBuffer);
      query.spatialRelationship = "intersects";
      query.geometry = geoBuffer;
      return osmParks.queryFeatures(query).then(displayResults);
    }
    // Execute a query to find Gov graphics within the buffer
    function findGovGraphics(inputLayer){
      var query = govParks.createQuery();
      //console.log(geoBuffer);
      query.spatialRelationship = "intersects";
      query.geometry = geoBuffer;
      return govParks.queryFeatures(query).then(displayResults);
    }
    function displayResults(results) {
      console.log("query finish");
      //resultsLayer.removeAll();
      var colorArray;
      var isGov = false;
      //console.log(results.features);
      // If the data is government, your render color is RED
      if (results.features[0].layer=== govParks){// gov data, render like that
        colorArray = [255,0,0,0.9];
        isGov = true;
      }
      // if the data is OSM, the render color is YELLOW
      else {
        colorArray = [51,51,204,0.9];
      }
      // Set the graphic property for each feature based on colorArray
      var features = results.features.map(function(graphic) {
        graphic.symbol = {
          type: "simple-fill",  // autocasts as new SimpleFillSymbol()
          color: colorArray,
          style: "solid",
          outline: {  // autocasts as new SimpleLineSymbol()
            color: "white",
            width: 1
          }
        };
        graphic.geometry = graphic.geometry;
        return graphic;
      });

      // Two arrays determine which graphic set they belong to - this is used for rendering
      //resultsLayer.addMany(features);
      if (isGov){
        //console.log("push");
        govParkGraphics = features;
      }
      else{
        //console.log("push2");
        osmParkGraphics = features;
      }
    }

    //TODO DEPRECATE?
    // If an OSM feat overlaps a government feature, add to geoDiff array
    // Return the overlapping feature's difference relative to the matched graphic.
    function findGeoDifferenceOSM(){
      // osmParkGraphics and //govParkGraphics comparison
      // which one has most features
      let geoDiff = [];
      for (x = 0; x < govParkGraphics.length; x++){

        //console.log(govParkGraphics[x]);

        for (y = 0; y < osmParkGraphics.length; y++){

          if (geometryEngine.intersects(govParkGraphics[x].geometry, osmParkGraphics[y].geometry)){
            // Show the difference
            //console.log(geometryEngine.difference(govParkGraphics[x].geometry,osmParkGraphics[y].geometry));
            geoDiff.push(geometryEngine.difference(govParkGraphics[x].geometry,osmParkGraphics[y].geometry));
          }
        }
      }

      return geoDiff;


    }

    // If a gov feat overlaps a osm feature, add to geoDiff array
    // Return the overlapping feature's difference relative to the matched graphic.
    function findGeoDifferenceGov(){
      // osmParkGraphics and //govParkGraphics comparison
      // which one has most features
      let geoDiff = [];
      osmGraphicCount = osmParkGraphics.length;
      govGraphicCount = govParkGraphics.length;
      //console.log("test");
      for (x = 0; x < govParkGraphics.length; x++){
        hasOverlap = false;

        for (y = 0; y < osmParkGraphics.length; y++){

          if (geometryEngine.intersects(govParkGraphics[x].geometry.extent, osmParkGraphics[y].geometry.extent)){
            // Show the difference
            overlapCounter++;
            hasOverlap = true;
            try{/*console.log(geometryEngine.difference(osmParkGraphics[x].geometry,govParkGraphics[y].geometry));*/
            geoDiff.push(geometryEngine.difference(osmParkGraphics[x].geometry,govParkGraphics[y].geometry));}
            catch(err){console.log(err)};
            break;
          }
          
        }
        // No government graphic overlapped an OSM graphic
        if(!hasOverlap){
          govCounter++;
        }
      }

      // Count of non-overlapping osm graphics
      findOSMOverlapGraphics();

      return geoDiff;


    }

    function findOSMOverlapGraphics(){

      for (x = 0; x < osmParkGraphics.length; x++){
        hasOverlap = false;
        for (y = 0; y < govParkGraphics.length; y++){

          if (geometryEngine.intersects(osmParkGraphics[x].geometry, govParkGraphics[y].geometry)){
            // Move on to the next OSM graphic
            //console.log("overlap, skip");
            hasOverlap = true;
            break;
          }
        }
        if (!hasOverlap){
          // No OSM graphic overlapped a government graphic
        //console.log("add osm missing");
        osmCounter++;

        }
        
      }

    }

    // Take the new graphics with differences and add them to the map
    function renderDiffOSM(results){
      //console.log("add graphics");
      //console.log(geoDiffSym);

      // get the geometries as graphics
      inputGraphics = [];
      for (x = 0; x < results.length; x++){

        inputGraphics.push(new Graphic(results[x],geoDiffSym));

      };

      mapView.graphics.addMany(inputGraphics);
    }

    // Take the new graphics with differences and add them to the map
    function renderDiffGov(results){
      //console.log("add graphics");
      //console.log(geoDiffSymGov);

      // get the geometries as graphics
      inputGraphics = [];
      for (x = 0; x < results.length; x++){

        inputGraphics.push(new Graphic(results[x],geoDiffSymGov));

      };

      mapView.graphics.addMany(inputGraphics);
    }

    // Function to help transition between modes on the map.
    function clearMap(evtParam){

      // Status tab set here. Listener then checks statusTab and returns if the tab is already selected
      // (Before this was creating a second time slider widget for example)
      statusTab = evtParam;

      if (featureCompareEvent){
        featureCompareEvent.pause();
      }

      // Remove all pre-existing graphics
      mapView.graphics.removeAll();

      // For each param
      // clear layers and graphics
      // add back only the layers of interest
      // for each feature

      // analysisTab - OSM time slider
      // editTab - Gov Parks and Trails
      // swipeTab - All layers with swipe widget
      // featureTab - All layers

      // Widgets to keep in mind
      // TimeSlider
      // Swipe
      // Editor

      // Remove all layers
      console.log(mapView.layers);
      console.log(mapView.ui);
      console.log(mapView.ui.components);

      let newLayers = [];

      // MapView has a ui function that exposes all widgets
      // ex view.ui.remove("compass")
      // ex view.ui.empty("bottom-left") - remove all from a part of map
      // can pass array of components
      // view.ui.components = ["attribution"]


      switch(evtParam){
        case "analysisTab":
          // osm layers only
          // allSourceLayers
          // if map has OSM layer, keep it, otherwise, remove Gov layer
          // if map does NOT have OSM layer, add it via allSourceLayer iteration
          osmTrailsLayerView.effect = null; // remove all effects (from timeslider)
          osmParksLayerView.effect = null; // remove all effects (from timeslider)
          osmFootprintsLayerView.effect = null; // remove all effects (from timeslider)
          filterFeats("OSM");
          mapView.ui.remove(editor);
          swipe.destroy();
          mapView.ui.remove(layerList); // layer list is removed for the OSM slider
          break;
        case "editTab":
          filterFeats("Gov");
          swipe.destroy();
          //mapView.ui.remove(layerList);
          osmTrailsLayerView.effect = null; // remove all effects (from timeslider)
          osmParksLayerView.effect = null; // remove all effects (from timeslider)
          osmFootprintsLayerView.effect = null; // remove all effects (from timeslider)
          mapView.ui.remove(timeSlider);
          break;
        case "swipeTab":
          resetFeats();
          osmTrailsLayerView.effect = null; // remove all effects (from timeslider)
          osmParksLayerView.effect = null; // remove all effects (from timeslider)
          osmFootprintsLayerView.effect = null; // remove all effects (from timeslider)
          mapView.ui.remove(editor);
          mapView.ui.remove(timeSlider);
          swipe = initSwipe();
          mapView.ui.add(layerList,{position:"bottom-left"});
          break;
        case "featureTab":
          resetFeats();
          osmTrailsLayerView.effect = null; // remove all effects (from timeslider)
          osmParksLayerView.effect = null; // remove all effects (from timeslider)
          osmFootprintsLayerView.effect = null; // remove all effects (from timeslider)
          mapView.ui.remove(editor);
          mapView.ui.remove(timeSlider);
          swipe.destroy();
          //mapView.ui.remove(layerList);
          break;
        default:
          console.log("baller");

      }


    }

    function initSwipe(){
      return new Swipe({
        leadingLayers: [govParks,govTrails,govFootprints],
        trailingLayers: [osmParks, osmTrails,osmFootprints],
        position: 50,
        view: mapView
      });
    }

    function resetFeats(){
      map.layers.forEach(function(inputLayer){
        inputLayer.visible = true;
      })
    }

    function filterFeats(inputLayerType){

      map.layers.forEach(function(inputLayer){

        if (inputLayer.type === "feature"){
          if(inputLayer.title.includes(inputLayerType)){
            inputLayer.visible = true;
          }
          else{
            inputLayer.visible = false;
          }
        }

      });
    }

    function initTimeSlider(){

      // initialize timeslider (was showing up when loading);
      timeSlider = new TimeSlider({
        container: "timeSlider",
        playRate:50,
        stops: {
          interval:{
            value:1,
            unit:"months"
          }
        }
      });

      //mapView.ui.add(timeSlider,"bottom-left");
      let layerView;
      let layerViewParks;
      //osmTrailsLayerView
      //osmParksLayerView
      //osmFootprintsLayerView

      const start = new Date(2010,1,24);
      timeSlider.fullTimeExtent = {
        start:start,
        end: osmTrails.timeInfo.fullTimeExtent.end
      };

      const end = new Date(start);
      end.setDate(end.getDate() + 30);

      timeSlider.values = [start,end];

      // watch for extent change
      timeSlider.watch("timeExtent",function(){
        //osmTrails.definitionExpression = "surface = 'paved'";
        // definition expression did not work, used this:
        // https://developers.arcgis.com/javascript/latest/api-reference/esri-tasks-support-Query.html#timeExtent
        var timeQuery = new Query({timeExtent:timeSlider.timeExtent});
        osmTrailsLayerView.queryFeatures(timeQuery).then(function(featureSet){

          // query on parks as well
          osmParksLayerView.queryFeatures(timeQuery).then(function(featureSet2){

            osmFootprintsLayerView.queryFeatures(timeQuery).then(function(featureSet3){

            osmTrailsLayerView.effect = {
              filter: {
                timeExtent: timeSlider.timeExtent,
                geometry: mapView.extent
              },
              excludedEffect: "grayscale(20%) opacity(12%)"
            };

            osmParksLayerView.effect = {
              filter: {
                timeExtent: timeSlider.timeExtent,
                geometry: mapView.extent
              },
              excludedEffect: "grayscale(20%) opacity(12%)"
            };

            osmFootprintsLayerView.effect = {
              filter: {
                timeExtent: timeSlider.timeExtent,
                geometry: mapView.extent
              },
              excludedEffect: "grayscale(20%) opacity(12%)"
            };

          })


          });

        });


      });
    }

    function initFeatCompChart(){
      // How many features overlapped (matching)
      // How many had no match (OSM)
      // How many had no match (Gov)
      // Sum of the features
      // Create a donut chart that gets added to a layerlist within a div
      //console.log("OSM GRAPHICS " + osmGraphicCount);
      //console.log("GOV GRAPHICS " + govGraphicCount);
      //console.log("UNIQUE GOV " + govCounter);
      //console.log("UNIQUE OSM " + osmCounter);
      //console.log("UNIQUE OVERLAP " + overlapCounter);
      //console.log("SUMMED " + govCounter + osmCounter + overlapCounter);

      var chartObj = query("#chartjs")[0].getContext('2d');
      //console.log(chartObj);

      data = {
        datasets: [{
            data: [overlapCounter, osmCounter, govCounter],
            //backgroundColor: ["#173F5F","#3CAEA3","#F6D55C"],
            backgroundColor: ["#f5f5f5","#d8b365","#5ab4ac"],
            borderColor: ["#141414","#141414","#141414"]
            //borderColor: ["#808080","#808080","#808080"]
        }],
    
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Shared Features',
            'OpenStreetMap',
            'Fairfax County Gov'
        ]
    }; 

       if(myDoughnutChart){
        myDoughnutChart.destroy();
       }

       myDoughnutChart = new Chart(chartObj, {
        type: 'doughnut',
        data: data,
      });




    }

    // Set the counts of overlapping/non-overlapping feats to 0
    function clearCounts(){

      govCounter = 0;
      osmCounter = 0;
      overlapCounter = 0;
      govGraphicCount = 0;
      osmGraphicCount = 0;

    }




    // Popup and panel sync
    mapView.when(function(){
      CalciteMapArcGISSupport.setPopupPanelSync(mapView);
    });

    // Search - add to navbar
    var searchWidget = new Search({
      container: "searchWidgetDiv",
      view: mapView
    });
    CalciteMapArcGISSupport.setSearchExpandEvents(searchWidget);

    // Map widgets
    var home = new Home({
      view: mapView
    });
    mapView.ui.add(home, "top-left");

    /*
    var zoom = new Zoom({
      view: mapView
    });
    mapView.ui.add(zoom, "top-left");
    */

    var compass = new Compass({
      view: mapView
    });
    mapView.ui.add(compass, "top-left");

    var basemapToggle = new BasemapToggle({
      view: mapView,
      secondBasemap: "satellite"
    });
    mapView.ui.add(basemapToggle, "bottom-right");

    var scaleBar = new ScaleBar({
      view: mapView
    });
    mapView.ui.add(scaleBar, "bottom-left");


    var attribution = new Attribution({
      view: mapView
    });
    mapView.ui.add(attribution, "manual");

    // Panel widgets - add legend
    var legendWidget = new Legend({
      container: "legendDiv",
      view: mapView
    });

});
