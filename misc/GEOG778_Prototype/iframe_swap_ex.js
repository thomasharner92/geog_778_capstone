<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="initial-scale=1,maximum-scale=1,user-scalable=no"
    />
    <title>Swap web maps in the same view - 4.12</title>

    <style>
      html,
      body {
        padding: 0;
        margin: 0;
        height: 100%;
        width: 100%;
        overflow: hidden;
      }

      #viewDiv, .embed-container {
        position: absolute;
        right: 0;
        left: 0;
        top: 60px;
        bottom: 0;
      }

      .header {
        position: absolute;
        top: 0;
        width: 100%;
        height: 10%;
      }

      .btns {
        margin: 0 auto;
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        overflow: auto;
      }

      .btn-switch {
        flex-grow: 4;
        background-color: rgba(34, 111, 14, 0.5);
        color: #fff;
        margin: 1px;
        width: 50%;
        padding: 20px;
        overflow: auto;
        text-align: center;
        cursor: pointer;
      }

      .active-map {
        color: #fff;
        background-color: rgba(34, 111, 14, 1);
      }
    </style>

    <link
      rel="stylesheet"
      href="https://js.arcgis.com/4.12/esri/themes/light/main.css"
    />
    <script src="https://js.arcgis.com/4.12/"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


    <script>
      require(["esri/views/MapView", "esri/WebMap"], function(MapView, WebMap) {
        var webmapids = [
          "ad5759bf407c4554b748356ebe1886e5",
          "71ba2a96c368452bb73d54eadbd59faa"

        ];

        $(".embed-container").hide();

        /************************************************************
         * Create multiple WebMap instances
         ************************************************************/
        var webmaps = webmapids.map(function(webmapid) {
          return new WebMap({
            portalItem: {
              // autocasts as new PortalItem()
              id: webmapid
            }
          });
        });

        /************************************************************
         * Initialize the View with the first WebMap
         ************************************************************/
        var view = new MapView({
          map: webmaps[0],
          container: "viewDiv"
        });

        document
          .querySelector(".btns")
          .addEventListener("click", function(event) {
            /************************************************************
             * On a button click, change the map of the View
             ************************************************************/
            var id = event.target.getAttribute("data-id");
            if (id === "1" || id ==="0") {
              $("#viewDiv").show();
              $(".embed-container").hide();
              var webmap = webmaps[id];
              view.map = webmap;
            }
            else{
              $("#viewDiv").hide();
              $(".embed-container").show();
            }
            var nodes = document.querySelectorAll(".btn-switch");
              for (var idx = 0; idx < nodes.length; idx++) {
                var node = nodes[idx];
                var mapIndex = node.getAttribute("data-id");
                if (mapIndex === id) {
                  node.classList.add("active-map");
                } else {
                  node.classList.remove("active-map");
                }
              }

          });
      });
    </script>
  </head>

  <body>
    <section class="header esri-widget">
      <div class="btns">
        <div class="btn-switch active-map" data-id="0">Missing Migrants</div>
        <div class="btn-switch" data-id="1">Refugee Routes</div>
        <div class="btn-switch" data-id="2">2015 European Sea Arrivals</div>
      </div>
    </section>
    <div id="viewDiv" class="esri-widget"></div>
    <div class="embed-container">
          <iframe
            width="2000"
            height="2000"
            frameborder="0"
            scrolling="no"
            marginheight="0"
            marginwidth="0"
            src="https://uw-mad.maps.arcgis.com/apps/StorytellingSwipe/index.html?appid=58b87d892682475384c696e22d3dd8d7">
          </iframe>
        </div>
  </body>
</html>
