require([
    // ArcGIS
    "esri/Map",
    "esri/layers/FeatureLayer",
    "esri/layers/GraphicsLayer",
    "esri/views/MapView",
    "esri/PopupTemplate",

    // Widgets
    "esri/widgets/Home",
    "esri/widgets/Zoom",
    "esri/widgets/Compass",
    "esri/widgets/Search",
    "esri/widgets/Legend",
    "esri/widgets/BasemapToggle",
    "esri/widgets/ScaleBar",
    "esri/widgets/Attribution",
    "esri/widgets/Swipe",
    "esri/widgets/Editor",
    "esri/widgets/Popup",
    "esri/widgets/TimeSlider",
    "esri/tasks/support/Query",
    "esri/geometry/geometryEngine",
    "esri/geometry/geometryEngineAsync",
    "esri/Graphic",
    "esri/Color",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/renderers/SimpleRenderer",
    "bootstrap/Collapse",
    "bootstrap/Dropdown",

    // Calcite Maps
    "calcite-maps/calcitemaps-v0.10",

    // Calcite Maps ArcGIS Support
    "calcite-maps/calcitemaps-arcgis-support-v0.10",
    "dojo/on",
    "dojo/dom",
    "dojo/domReady!"
  ], function(Map, FeatureLayer,GraphicsLayer,MapView,PopupTemplate, Home, Zoom, Compass, Search, Legend, BasemapToggle, ScaleBar, Attribution, Swipe,Editor,Popup,TimeSlider,Query,geometryEngine,geometryEngineAsync,Graphic,Color,SimpleFillSymbol, SimpleLineSymbol, SimpleRenderer,Collapse, Dropdown, CalciteMaps, CalciteMapArcGISSupport,on,dom) {

    /******************************************************************
     *
     * Create the map, view and widgets
     *
     ******************************************************************/
     var closeSwipe = dom.byId("swipeOff");
     var editTab = dom.byId("editNav");
     var swipeTab = dom.byId("swipeNav");
     var analysisTab = dom.byId("analysisNav");
     var featureTab = dom.byId("featHunter");

     isOsm = true;

     var resultsLayer = new GraphicsLayer();

     // Buffer color
     var buffSym = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 255, 255, 1]), 3), null);
     var buffSymFade = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 255, 255, 0.4]), 10), null);
     var geoDiffSym = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 255, 0, 0.5]), 2), null);
     var geoDiffSymGov = new SimpleFillSymbol(SimpleFillSymbol.STYLE_NULL, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0, 0.5]), 2), null);



     let osmParkGraphics;
     let govParkGraphics;
     let geoBuffer;

     var testTemplate = {
       title:"Stop",
       content: function(feature){
         return feature.attributes;
       }
   }

     var map = new Map({
       basemap: "gray-vector"
     });


     var osmParks = new FeatureLayer({
       url:"https://services.arcgis.com/HRPe58bUyBqyyiCt/arcgis/rest/services/Fairfax_OSM_Parks/FeatureServer",
       outFields: ["*"],
       //popupTemplate: testTemplate,
       title: "OSM_Parks"
     });

     var govParks = new FeatureLayer({
       url:"https://services.arcgis.com/HRPe58bUyBqyyiCt/arcgis/rest/services/Fairfax_County_Gov_Parks/FeatureServer",
       outFields:["*"],
       //popupTemplate: testTemplate,
       title: "Gov_Parks"
     });

     var osmTrails = new FeatureLayer({
       url:"https://services.arcgis.com/HRPe58bUyBqyyiCt/arcgis/rest/services/Fairfax_County_OSM_Trails/FeatureServer",
       outFields: ["*"],
       //popupTemplate: testTemplate,
       title: "OSM_Trails",
       timeInfo:{
         startField:"osm_timest",//date field
         interval: {
           unit:"months",
           value:1
         }
       }
     });

     var govTrails = new FeatureLayer({
       url:"https://services.arcgis.com/HRPe58bUyBqyyiCt/arcgis/rest/services/Fairfax_County_Gov_Trails/FeatureServer",
       outFields: ["*"],
       //popupTemplate: testTemplate,
       title: "Gov_Trails"
     });

     map.add(osmParks);
     map.add(osmTrails);
     map.add(govParks);
     map.add(govTrails);
     map.add(resultsLayer);


    // View
    var mapView = new MapView({
      container: "mapViewDiv",
      map: map,
      center: [-77.300227,38.843320],
      zoom: 12
      //padding: {
        //top: 50,
        //bottom: 0
      //},
      //ui: {components: []}
    });

    //mapView.defaultPopupTemplateEnabled = true;


    var swipe = new Swipe({
      leadingLayers: [govParks,govTrails],
      trailingLayers: [osmParks, osmTrails],
      position: 50,
      view: mapView
    });

    mapView.ui.add(swipe); // swipe is added by default

    // Editor to interface with County data
    let editor = new Editor({
      view: mapView
    });

    console.log(editor);

    // initialize timeslider, but don't add
    let timeSlider = new TimeSlider({
        container: "timeSlider",
        playRate:50,
        stops: {
          interval:{
            value:1,
            unit:"months"
          }
        }
      });

    // Remove swipe, add the editor widget
    on(editTab, "click", function(evt){
      clearMap("editTab");
      // does not working coming from "findFeatures" tab
      console.log("add editor");
      mapView.ui.add(editor,"bottom-right");

      /*let hasGov = false;
      // Remove all layers containing osm
      console.log(map.layers.length);
      try{
        map.remove(osmParks);
        map.remove(osmTrails);
      }catch(err){
        console.log(err);
      }


      if (map.layers.find(function(layer){
        if (layer.type != "graphics"){
        if (layer.title.includes("Gov")){
          hasGov = true;
        }
      }
      }));

      if (!hasGov){
        // add gov layers
        map.add(govParks);
        map.add(govTrails);
      }

        // destroy swipe instance
        swipe.destroy();
        try {
          console.log("remove time slider");
          mapView.ui.remove(timeSlider);
        }catch(err){console.log(err)};
        // Get that editor widget up
        mapView.ui.add(editor, "bottom-right");
      });*/
    });

    on(swipeTab,"click",function(evt){
      clearMap("swipeTab");
      // remove the editor widget
      /*let hasGov = false;
      let hasOSM = false;

      if (map.layers.find(function(layer){
        if (layer.type != "graphics"){
        if (layer.title.includes("Gov")){
          hasGov = true;
        }
        else if (layer.title.includes("OSM")){
          hasOSM = true;
        }
      }
      }));

      if (!hasGov){
        // add gov layers
        map.add(govParks);
        map.add(govTrails);
      }

      if (!hasOSM){
        // add back OSM
        map.add(osmParks);
        map.add(osmTrails);
      }*/


      mapView.ui.add(swipe);
    });

    on(analysisTab,"click",function(evt){
      // Remove swipe
      // Remove editor widget
      // Add OSM layer, and filtering
      clearMap("analysisTab");
      initTimeSlider();
      mapView.ui.add(timeSlider,"bottom-left");
      let hasLayer = false;

      let hasOSM = false;
      // Remove all gov layers
      try{
        map.remove(govParks);
        map.remove(govTrails);
      }catch(err){
        console.log(err);
      }

      console.log(map.layers);

      if (map.layers.find(function(layer){
        if (layer.type != "graphics"){
          if (layer.title.includes("OSM")){
            hasOSM = true;
          }
        }
      }));

      if (!hasOSM){
        // add gov layers
        map.add(osmParks);
        map.add(osmTrails);
      }


    });

    on(featureTab,"click",function(evt){
      clearMap("featureTab");
      // Create search window (buffer)
      // both layers
      // geometry engine
      // if OSM feat does not overlap Gov feat, highlight it

      // geodesic buffer
      // query layers(?)
      // if within buffer, return
      // use returned results to do overlaps geometryEngine function

      // initial test via swipe tab
      //swipe.destroy();
      //on(mapView, "click", createBuffer);

      on(mapView,"click",function(evt){

        // buffer calc
        // query feats within buffer bounds (osm and gov)
        // using resulting graphics, find overlapping feats, then a difference calc
        resultsLayer.removeAll();
        geometryEngineAsync.geodesicBuffer(evt.mapPoint,2,"miles")
        .then(addBufferGraphic)
        .then(findOSMGraphics)
        .then(findGovGraphics)
        .then(findGeoDifferenceOSM)
        .then(renderDiffOSM)
        .then(findGeoDifferenceGov)
        .then(renderDiffGov)
        .catch(function(err){console.error(err)});

      });

    });

    function addBufferGraphic(bufferedGeo){
      geoBuffer = bufferedGeo;
      return mapView.graphics.add(new Graphic(bufferedGeo, buffSymFade));
    }
    function findOSMGraphics(inputLayer){
      var query = osmParks.createQuery();
      console.log(geoBuffer);
      query.spatialRelationship = "intersects";
      query.geometry = geoBuffer;
      return osmParks.queryFeatures(query).then(displayResults);
    }
    function findGovGraphics(inputLayer){
      var query = govParks.createQuery();
      console.log(geoBuffer);
      query.spatialRelationship = "intersects";
      query.geometry = geoBuffer;
      return govParks.queryFeatures(query).then(displayResults);
    }

    function displayResults(results) {
      //resultsLayer.removeAll();
      var colorArray;
      var isGov = false;
      console.log(results.features);
      if (results.features[0].layer=== govParks){// gov data, render like that
        colorArray = [255,0,0,0.9];
        isGov = true;
      }
      else {
        colorArray = [51,51,204,0.9];
      }
      var features = results.features.map(function(graphic) {
        graphic.symbol = {
          type: "simple-fill",  // autocasts as new SimpleFillSymbol()
          color: colorArray,
          style: "solid",
          outline: {  // autocasts as new SimpleLineSymbol()
            color: "white",
            width: 1
          }
        };
        return graphic;
      });

      //resultsLayer.addMany(features);
      if (isGov){
        console.log("push");
        govParkGraphics = features;
      }
      else{
        console.log("push2");
        osmParkGraphics = features;
      }
    }

    function findGeoDifferenceOSM(){
      // osmParkGraphics and //govParkGraphics comparison
      // which one has most features
      let geoDiff = [];
      for (x = 0; x < govParkGraphics.length; x++){

        for (y = 0; y < osmParkGraphics.length; y++){

          if (geometryEngine.overlaps(govParkGraphics[x].geometry, osmParkGraphics[y].geometry)){
            // Show the difference
            console.log(geometryEngine.difference(govParkGraphics[x].geometry,osmParkGraphics[y].geometry));
            geoDiff.push(geometryEngine.difference(govParkGraphics[x].geometry,osmParkGraphics[y].geometry));
          }
        }
      }

      return geoDiff;


    }

    function findGeoDifferenceGov(){
      // osmParkGraphics and //govParkGraphics comparison
      // which one has most features
      let geoDiff = [];
      console.log("test");
      for (x = 0; x < govParkGraphics.length; x++){

        for (y = 0; y < osmParkGraphics.length; y++){

          if (geometryEngine.overlaps(govParkGraphics[x].geometry, osmParkGraphics[y].geometry)){
            // Show the difference
            try{console.log(geometryEngine.difference(osmParkGraphics[x].geometry,govParkGraphics[y].geometry));
            geoDiff.push(geometryEngine.difference(osmParkGraphics[x].geometry,govParkGraphics[y].geometry));}
            catch(err){console.log(err)};
          }
        }
      }

      return geoDiff;


    }

    function renderDiffOSM(results){
      console.log("add graphics");
      console.log(geoDiffSym);

      // get the geometries as graphics
      inputGraphics = [];
      for (x = 0; x < results.length; x++){

        inputGraphics.push(new Graphic(results[x],geoDiffSym));

      };

      mapView.graphics.addMany(inputGraphics);
    }

    function renderDiffGov(results){
      console.log("add graphics");
      console.log(geoDiffSymGov);

      // get the geometries as graphics
      inputGraphics = [];
      for (x = 0; x < results.length; x++){

        inputGraphics.push(new Graphic(results[x],geoDiffSymGov));

      };

      mapView.graphics.addMany(inputGraphics);
    }

    function clearMap(evtParam){

      // For each param
      // clear layers and graphics
      // add back only the layers of interest
      // for each feature

      // analysisTab - OSM time slider
      // editTab - Gov Parks and Trails
      // swipeTab - All layers with swipe widget
      // featureTab - All layers

      // Widgets to keep in mind
      // TimeSlider
      // Swipe
      // Editor

      // Remove all layers
      console.log(mapView.layers);
      console.log(mapView.ui);
      console.log(mapView.ui.components);

      // MapView has a ui function that exposes all widgets
      // ex view.ui.remove("compass")
      // ex view.ui.empty("bottom-left") - remove all from a part of map
      // can pass array of components
      // view.ui.components = ["attribution"]


      switch(evtParam){
        case "analysisTab":
          mapView.ui.remove(editor);
          swipe.destroy();
          break;
        case "editTab":
          swipe.destroy();
          mapView.ui.remove(timeSlider);
          break;
        case "swipeTab":
          mapView.ui.remove(editor);
          mapView.ui.remove(timeSlider);
          swipe = initSwipe();
          break;
        case "featureTab":
          mapView.ui.remove(editor);
          mapView.ui.remove(timeSlider);
          swipe.destroy();
          break;
        default:
          console.log("baller");

      }


    }

    function initSwipe(){
      return new Swipe({
        leadingLayers: [govParks,govTrails],
        trailingLayers: [osmParks, osmTrails],
        position: 50,
        view: mapView
      });
    }

    function initTimeSlider(){

      //mapView.ui.add(timeSlider,"bottom-left");
      let layerView;
      let layerViewParks;

      mapView.whenLayerView(osmTrails).then(function(lv){
      layerView = lv;
      const start = new Date(2010,1,24);
      timeSlider.fullTimeExtent = {
        start:start,
        end: osmTrails.timeInfo.fullTimeExtent.end
      };

      const end = new Date(start);
      end.setDate(end.getDate() + 30);

      timeSlider.values = [start,end];
    });

    // Create layer view for parks so they are also
    // included in time slider range widget
    mapView.whenLayerView(osmParks).then(function(lv){
    layerViewParks = lv;
    });

      // watch for extent change
      timeSlider.watch("timeExtent",function(){
        //osmTrails.definitionExpression = "surface = 'paved'";
        // definition expression did not work, used this:
        // https://developers.arcgis.com/javascript/latest/api-reference/esri-tasks-support-Query.html#timeExtent
        var timeQuery = new Query({timeExtent:timeSlider.timeExtent});
        layerView.queryFeatures(timeQuery).then(function(featureSet){

          // query on parks as well
          layerViewParks.queryFeatures(timeQuery).then(function(featureSet2){

            layerView.effect = {
              filter: {
                timeExtent: timeSlider.timeExtent,
                geometry: mapView.extent
              },
              excludedEffect: "grayscale(20%) opacity(12%)"
            };

            layerViewParks.effect = {
              filter: {
                timeExtent: timeSlider.timeExtent,
                geometry: mapView.extent
              },
              excludedEffect: "grayscale(20%) opacity(12%)"
            };


          });

        });


      });
    }




    // Popup and panel sync
    mapView.when(function(){
      CalciteMapArcGISSupport.setPopupPanelSync(mapView);
    });

    // Search - add to navbar
    var searchWidget = new Search({
      container: "searchWidgetDiv",
      view: mapView
    });
    CalciteMapArcGISSupport.setSearchExpandEvents(searchWidget);

    // Map widgets
    var home = new Home({
      view: mapView
    });
    mapView.ui.add(home, "top-left");

    var zoom = new Zoom({
      view: mapView
    });
    mapView.ui.add(zoom, "top-left");

    var compass = new Compass({
      view: mapView
    });
    mapView.ui.add(compass, "top-left");

    var basemapToggle = new BasemapToggle({
      view: mapView,
      secondBasemap: "satellite"
    });
    mapView.ui.add(basemapToggle, "bottom-right");

    var scaleBar = new ScaleBar({
      view: mapView
    });
    mapView.ui.add(scaleBar, "bottom-left");


    var attribution = new Attribution({
      view: mapView
    });
    mapView.ui.add(attribution, "manual");

    // Panel widgets - add legend
    var legendWidget = new Legend({
      container: "legendDiv",
      view: mapView
    });

});
